from random import randint

name = input ("Hi! What is your name? ")

for guess_num in range(1, 6):
    month_num = randint(1, 12)
    year_num = randint(1924, 2004)

    print("Guess", guess_num, name.title(), "were you born in",
            month_num, "/", year_num, "?")

    response = input("Yes or No? - ").title()


    if response == "Yes":
        print("I knew it!!")
        exit()
    elif guess_num == "6":
        print("OK... FINE YOU WIN.")
    else:
        print("Okay..lemme try again")
    print()
